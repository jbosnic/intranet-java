import React from 'react';
import {Link} from 'react-router-dom';
import {FormattedMessage, formatMessage, injectIntl } from 'react-intl';

const Nav = ({authenticated, navigationButton}) => {
    return (<ul className="a-nav a-nav--section">
        <li><a href="#"><FormattedMessage id='topNav.quickAccess'/></a>
            <ul className="a-list_twocolumns">
                <li><a target="_blank" href="http://webmail.adistec.com/"><FormattedMessage id='topNav.webmailAdistec'/></a></li>
                <li><a target="_blank" href="http://www.netsuite.com"><FormattedMessage id='topNav.netsuiteLogin'/></a></li>
                <li><a target="_blank" href="http://www.Google.com"><FormattedMessage id='topNav.google'/></a> </li>
            </ul>
        </li>
        {authenticated ?
            <li><a href="#"><FormattedMessage id='topNav.graphicResources'/></a>
                <ul className="a-list_twocolumns">
                    <li><a href="http://dsn.marketing.adistec.com//material_corp/logotipo_corporativo.zip" target="_blank"><FormattedMessage id='topNav.corporateLogo'/></a></li>
                    <li><a href="http://dsn.marketing.adistec.com//material_corp/tipografias_institucionales.zip" target="_blank"><FormattedMessage id='topNav.corporateFonts'/></a></li>
                    <li><a href="http://dsn.marketing.adistec.com//material_corp/hojas_membretadas.zip" target="_blank"><FormattedMessage id='topNav.letterhead'/></a></li>
                    <li><a href="http://dsn.marketing.adistec.com//material_corp/adistec_hoja_membretada_A4_educacion.docx.zip" target="_blank"><FormattedMessage id='topNav.letterheadEducation'/></a></li>
                    <li><a href="http://dsn.marketing.adistec.com//material_corp/adistec_hoja_membretada_A4_aps.docx.zip" target="_blank"><FormattedMessage id='topNav.letterheadAPS'/></a></li>
                    <li><a href="http://dsn.marketing.adistec.com//material_corp/adistec_hoja_membretada_A4_acs.docx.zip" target="_blank"><FormattedMessage id='topNav.letterheadACS'/></a></li>
                    <li><a href="http://dsn.marketing.adistec.com//material_corp/adistec_presentacion_corporativa_enero_2017.zip" target="_blank"><FormattedMessage id='topNav.corporatePresentationJan2017_es_en'/></a></li>
                    <li><a href="http://dsn.marketing.adistec.com//material_corp/template_presentaciones_ppt.zip" target="_blank"><FormattedMessage id='topNav.pttPresentations'/></a> </li>
                    <li><a href="http://dsn.marketing.adistec.com//material_corp/firma_para_emails.zip" target="_blank"><FormattedMessage id='topNav.emailSignatures'/></a> </li>
                    <li><a href="http://dsn.marketing.adistec.com//material_corp/adistec_linkedin_banner.zip" target="_blank"><FormattedMessage id='topNav.linkedinBanner'/></a> </li>
                    <li><a href="http://dsn.marketing.adistec.com/pdf/brochure_adistec.pdf" target="_blank"><FormattedMessage id='topNav.corporateBrochureArg'/></a> </li>
                    <li><a href="http://dsn.marketing.adistec.com//material_corp/manual_de_marca.zip" target="_blank"><FormattedMessage id='topNav.brandGuidelines'/></a> </li>
                    <li><a href="http://dsn.marketing.adistec.com//material_corp/aec_logo.zip" target="_blank"><FormattedMessage id='topNav.aecLogo'/></a></li>
                    <li><a href="http://dsn.marketing.adistec.com//material_corp/aec_template_word.zip" target="_blank"><FormattedMessage id='topNav.aecTemplateWord'/></a></li>
                </ul></li> : null}
        <li><a href="#">Adistec Enterprise Cloud</a>
            <ul className="a-list_twocolumns">
                <li><a target="_blank" href="http://www.adistec.com/documents/App_2_AdistecPasswordPolicy_11022016EN.PDF"><FormattedMessage id='topNav.aecPasswordPolicy'/>/ 2016</a></li>
                <li><a target="_blank" href="http://www.adistec.com/documents/App_2_AdistecPasswordPolicy_11022016EN_Signed.pdf"><FormattedMessage id='topNav.aecPasswordPolicySigned'/>/ 2016</a></li>
                <li><a target="_blank" href="http://www.adistec.com/documents/App_8_BackupInformationPolicy_11022016_EN.DOCX"><FormattedMessage id='topNav.aecBackupPolicy'/>/ 2016</a></li>
                <li><a target="_blank" href="http://www.adistec.com/documents/App_8_BackupInformationPolicy_11022016_EN_Signed.pdf"><FormattedMessage id='topNav.aecBackupPolicySigned'/>/ 2016</a></li>
                <hr className="hr-1" />
                <li><a target="_blank" href="http://www.adistec.com/documents/AEC_4.2.6_Inform_Sec_Mgt_Policy_EN.pdf">AEC Information Security Policy (English)/ 2019</a></li>
                <li><a target="_blank" href="http://www.adistec.com/documents/AEC_4.2.6_Inform_Sec_Mgt_Process_EN.pdf">AEC Information Security Management Process (English)/ 2019</a></li>
                <li><a target="_blank" href="http://www.adistec.com/documents/AEC_4.2.6_Password_Policy_EN.pdf">AEC Password Policy (English)/ 2019</a></li>
            </ul>
        </li>
        {navigationButton ? navigationButton : null}
    </ul>)
}
export default Nav;