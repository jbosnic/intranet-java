import { APP_CONFIGURATION, APP_CONFIGURATION_ERROR } from '../actions/types';

const INITIAL_STATE = {error: '', appConfig: {}};

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case APP_CONFIGURATION:
            return {...state, error: '', appConfig: action.payload};
        case APP_CONFIGURATION_ERROR:
            return {...state, error: action.payload};
    }

    return state;
}