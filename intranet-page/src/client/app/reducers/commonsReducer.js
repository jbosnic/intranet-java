import { 
	GET_EMPLOYEES_SUCCESS, 
	GET_EMPLOYEES_FAILED , 
	GET_BRANCHOFFICES_SUCCESS,
	GET_BRANCHOFFICES_FAILED,
    GET_AREAS_SUCCESS,
    GET_AREAS_FAILED
} from '../actions/types';

const INITIAL_STATE = {employees: [],employeesError:null, offices:[],officesError:null,areas:[],areasError:null};

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case GET_EMPLOYEES_SUCCESS:
            return {...state, employees:action.payload};
        case GET_EMPLOYEES_FAILED:
            return {...state, employeesError: action.payload};
        case GET_BRANCHOFFICES_SUCCESS:
            return {...state, offices:action.payload};
        case GET_BRANCHOFFICES_FAILED:
            return {...state, officesError: action.payload};
        case GET_AREAS_SUCCESS:
            return {...state, areas:action.payload};
        case GET_AREAS_FAILED:
            return {...state, areasError: action.payload};
    }
    return state;
}
