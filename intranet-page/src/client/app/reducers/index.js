import { combineReducers } from 'redux';
import userReducer from './userReducer';
import loaderReducer from './loaderReducer';
import authReducer from './authReducer';
import appConfigReducer from './appConfigReducer';
import commonsReducer from './commonsReducer';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
	form: formReducer,
    user: userReducer,
    loader:loaderReducer,
	auth: authReducer,
	commons: commonsReducer,
	appConfig: appConfigReducer
});

export default rootReducer;
