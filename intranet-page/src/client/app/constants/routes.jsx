import ReferidosForm from '../containers/ReferidosForm.jsx';
import MapaEmpleados from '../containers/MapaEmpleados.jsx';

import WithTracker from '../containers/WithTracker.jsx';

const routes =[
	{
		path: '/',
		component: WithTracker(MapaEmpleados)
	},
	{
		path: '/referidos',
		component: WithTracker(ReferidosForm)
	}
];

export default routes;