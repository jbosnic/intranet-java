import React from 'react';

class BasesContent extends React.Component{
    constructor(props) {
        super(props);
    }


    render(){
        return (

            <div>
                <div className="texto-corrido">
                    <h2>
                        <span className="title_highlight">Condiciones del Programa de Referidos</span>
                    </h2>
                    <h3>1. OBJETIVO</h3>
                    <p>El Programa de  Referidos tiene como propósito promover la participación de los colaboradores  recompensándolos por postular a candidatos en nuestros procesos de reclutamiento y  que a su vez éstos resulten contratados por la Organización. </p>
                    <h3>2. DEFINICIONES</h3>
                    <ul className="bulleted">
                        <li>Referido: el candidato será  un “referido” cuando un colaborador de Adistec no puede garantizar que el postulante cuenta con las habilidades y competencias para  desempeñarse correctamente en la posición.</li>
                        <li>Recomendado: el candidato  será un “recomendado” cuando un colaborador de Adistec asegure que el postulante podrá desempeñarse correctamente en la posición. </li>
                    </ul>
                    <h3>3. BENEFICIOS DEL  PROGRAMA</h3>
                    <ul className="bulleted">
                        <li>Mayor participación y  compromiso de los colaboradores.</li>
                        <li>Mayor conocimiento y  referencias sobre el candidato.</li>
                        <li>Obtención de perfiles  difíciles de captar.</li>
                        <li>Llegar a los estudiantes y  Jóvenes profesionales que no se postulan espontáneamente.</li>
                        <li>Buen rendimiento del referido  dado el vínculo ya establecido.</li>
                        <li>Nuevo canal de reclutamiento  para la empresa.</li>
                        <li>Elemento de motivación de  clima laboral con menor costo.</li>
                        <li>Disminuye costos en los  procesos de Reclutamiento y Selección.</li>
                    </ul>
                    <h3>4. &nbsp;ALCANCE</h3>
                    <p>Todos los  colaboradores de Adistec que cumplan una jornada a tiempo completo o tiempo  parcial son elegibles para recibir el incentivo por referir candidatos  calificados que sean contratados.</p>
                    <h3>5. CONDICIONES PARA REFERIR  PERFILES A BÚSQUEDAS ABIERTAS</h3>
                    <ul className="bulleted">
                        <li>Podrán referir candidatos,  todos aquellos colaboradores que cuenten con más de 3 meses de antigüedad a  excepción del equipo de Recursos Humanos, personas que participan en procesos  de reclutamiento y selección y responsables de la contratación y/o supervisión  del ingresante.<strong> </strong></li>
                        <li>La “referencia” de un  candidato tiene validez de 1 año a partir de que Recursos Humanos recibe el CV,  y cumplido este plazo no será tenido en cuenta dentro del Programa de  Referidos.<strong> </strong></li>
                        <li>Cuando un candidato es referido para ocupar una posición en una subsidiaria  distinta a la del colaborador, el incentivo será otorgado por el país al que pertenece  el colaborador que refirió el candidato.</li>
                        <li>El colaborador no debe tener ningún vínculo familiar con el  candidato referido.</li>
                    </ul>
                    <h3>6. ELEGIBILIDAD</h3>
                    <p>Los candidatos referidos podrán ser  contratados para posiciones en todas las unidades de negocio dentro de Adistec.  Sin embargo, no se pagará ningún tipo de recompensa por referir candidatos en  las siguientes circunstancias:</p>
                    <ul className="bulleted">
                        <li>Candidatos que hayan participado en procesos de selección de  Adistec en los últimos doce (12) meses.</li>
                        <li>Candidatos previamente referidos por otra fuente en los últimos  doce (12) meses.</li>
                        <li>Candidatos que esten actualmente empleados por canales o  fabricantes con los que tengamos un vinculo comercial. </li>
                        <li>Candidatos que esten actualmente empleados por distribuidores que  compitan directamente con Adistec.</li>
                        <li>Candidatos referidos que desempeñar una posición en la cual le  reporten directamente al empleado o empleada que les refirió.</li>
                        <li>Empleados temporales o contratados a través de terceras partes que  sean remitidos a Procesos de Selección para otras busquedas, en carácter de  referidos.</li>
                        <li>El empleado o empleada referente debe tener un conocimiento  personal del candidato que está refiriendo. </li>
                        <li>Los referidos serán tenidos en cuenta solo si  cumplen con los requisitos requeridos por el puesto y será RRHH quien determine  si avanzar con ese perfil en el proceso. </li>
                    </ul>

                    <p><strong><u>Nota</u></strong><strong>:</strong> La  acción de referir a empleados de Adistec como candidatos a otra posición dentro  de la Organización no se considerará parte del proceso de reclutamiento, de  esta manera se asegura un proceso de selección justo, igualitario e  independiente. </p>
                    <h3>6. ESQUEMA DE  INCENTIVO POR REFERENCIA</h3>
                    <ul className="bulleted">
                        <li>El colaborador que refiera un  candidato y el mismo sea contratado por Adistec, recibirá su premio incentivo a  los 90 días de producido el ingreso.</li>
                        <li>El premio será considerado No  Remunerativo a través de una Gift Card y el monto será definido según el  siguiente esquema:</li>
                        <li><div className="referidos-esquemaincentivo">
                            <table cellSpacing={0} cellPadding={0}>
                                <thead>
                                <tr>
                                    <th>Referido
                                    </th><th>Recomendado
                                </th>
                                </tr></thead>
                                <tbody>
                                <tr>
                                    <td><p align="center"><strong>$50</strong></p></td>
                                    <td><p align="center"><strong>$100</strong></p></td>
                                </tr>
                                </tbody>
                            </table>
                        </div></li>
                    </ul>
                    <p>Notas<strong>:</strong><br/><br/>
                        A partir de Julio de 2019 se manejara el siguiente esquema de premios para los Referidos o Recomendados
                        que ingresen al área de IS (Information Systems) en la subsidiaria de Argentina:
                        <br/><br/>

                        <span style={{display:'inline-block',border:'solid 1px #DDD',padding:'10px 15px'}}>
                            NetSuite Trainee
                            <strong>
                                USD 200
                            </strong>
                        </span><span style={{display:'inline-block',border:'solid 1px #DDD',padding:'10px 15px'}}>
                        Jr / Ssr / Sr
                        <strong>
                            USD 400
                        </strong>
                        </span>

                        <br/><br/>
                        Se considera el periodo de 90 días como el tiempo establecido para asegurarse que el desempeño de
                        quien ha sido referido sea el esperado.
                    </p>
                    <h3>7. PROCESO DEL PROGRAMA DE REFERIDOS</h3>
                    <div className="referidos-tablaproceso">
                        <table cellPadding={0} cellSpacing={0}>
                            <thead>
                                <tr>
                                    <th>Difusión de Búsqueda</th>
                                    <th>Referencia</th>
                                    <th>Selección</th>
                                    <th>Ingreso</th>
                                    <th>Medición</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <p>Piezas de difusión inicial de la búsqueda en la página de Adistec de LinkedIn</p>
                                        <p>Envío de Mailings a todos los colaboradores de la compañía a través del Addy News Mensual</p>
                                        <span className="material-icons flecha">label_important</span>
                                    </td>
                                    <td>
                                        <p>Recepción de referencias a través de la Intranet</p>
                                        <p>Feedback y agradecimiento al referente</p>
                                        <p>Contacto con el referido para invitarlo al proceso y validar vínculo con el referente</p>
                                        <span className="material-icons flecha">label_important</span>
                                    </td>
                                    <td>
                                        <p>Participación del referido en el proceso de selección (igual que todos sin distinciones)</p>
                                        <p>Sondeo del referido para alinear expectativas y relevar áreas de interés</p>
                                        <p>Asignación a búsquedas</p>
                                        <span className="material-icons flecha">label_important</span>
                                    </td>
                                    <td>
                                        <p>Notificación al Referente del Ingreso de su referido</p>
                                        <p>Entrega de premio a los 90 días de producido el ingreso (siempre que el referido se encuentre aún en la empresa)</p>
                                        <span className="material-icons flecha">label_important</span>
                                    </td>
                                    <td className="last">
                                        <p>% de búsquedas cubiertas por medio de esta fuente</p>
                                        <p>Tiempo de cobertura de la vacante bajo este esquema/tiempo promedio</p>
                                        <p>Calidad de los referidos (total referencias / total incorporados)</p>
                                        <p>Impacto (Referentes / Total de empleados)</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default BasesContent;