import {axios} from './axiosWrapper';
import { 
    APP_CONFIGURATION,
    APP_CONFIGURATION_ERROR
} from './types';

const API_URL = _API;

export function getAppConfiguration() {
    return function (dispatch) {
        axios.get('/app')
        .then(response => {
            dispatch({type: APP_CONFIGURATION,payload:response.data})
        }).catch(error => {
            dispatch({type: APP_CONFIGURATION_ERROR, payload: error.response});
        })
    }
}