import axiosClass from 'axios';
import {getKeycloak} from '../config/keycloak';

const instance = axiosClass.create({ baseURL: _API });

instance.interceptors.request.use(config => {
    if(getKeycloak().token){
        config.headers = {...config.headers, 'Authorization':'Bearer ' + getKeycloak().token};
    }
    return config;
}, undefined);


export let axios = instance;