import { axios } from './axiosWrapper';
import { USER_INFO, TOGGLE_MENU, AXIOS_REQUEST } from '../actions/types';

export const setUserInfo = (userInfo) => {
    return dispatch => {
        dispatch({type: USER_INFO, payload: userInfo});
    }
}


export const toggleMenu = () => {
    return dispatch => {
        dispatch({type: TOGGLE_MENU});
    }
};
