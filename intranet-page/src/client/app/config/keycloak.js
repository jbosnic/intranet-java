import Keycloak from 'keycloak-js';

const KeyCloackConfig = 
{
    url: _AUTH_URL,
    realm: 'Adistec',
    clientId: 'intranet-frontend',
    redirectUri: _REDIRECT_URI,
}

let keycloak = Keycloak(KeyCloackConfig);

keycloak.onTokenExpired = () => {
	keycloak.updateToken().success(function(refreshed) {
		keycloak.loadUserInfo().success(userInfo => { 
        	setUserInfo(userInfo);
        });
	}).error(function() {
	    keycloak.logout();
	});
}

export const getKeycloak = () => {
	return keycloak;
}


