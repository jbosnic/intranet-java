let path = require('path');
let webpack = require('webpack');
let BUILD_DIR = path.resolve(__dirname, '../src/server/static/js');
let APP_DIR = path.resolve(__dirname, '../src/client/app');
let SERVER_DIR = path.resolve(__dirname, '../src/server/static');

const webpackMerge = require('webpack-merge');
const commonConfig = require('./webpack-common.config.js');


module.exports = webpackMerge(commonConfig, {
    output: {
        path: SERVER_DIR,
        filename: '[name].js',
        chunkFilename: '[name].js',
        publicPath: '/'
    },
    plugins: [
        new webpack.DefinePlugin({
            _API: JSON.stringify('/intranet-api'),
            _ENV: JSON.stringify('dev'),
            _AUTH_URL: JSON.stringify('http://auth.sit.com/auth'),
            _REDIRECT_URI: JSON.stringify('localhost:3000')
        }),
        new webpack.HotModuleReplacementPlugin()
    ],
    devServer: {
        historyApiFallback: true,
        port: 3000,
        contentBase: SERVER_DIR,
        hot: true,
        proxy: [
            {
            context:['/intranet-api/**'],
            target: 'http://127.0.0.1:8080',
            secure: false,
            changeOrigin: true
            }
        ]
    },
    devtool:"source-map",
    watch: true
});