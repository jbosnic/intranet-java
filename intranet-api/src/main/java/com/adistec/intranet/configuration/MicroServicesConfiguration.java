package com.adistec.intranet.configuration;

import com.adistec.mailclient.impl.MailClient;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MicroServicesConfiguration {

    @Autowired
    private EurekaClient eurekaClient;

    @Value("${spring.services.endpoint.mail.appName}")
    private String mailAppName;

    @Value("${spring.services.endpoint.portal.appName}")
    private String portalAppName;

    @Value("${spring.services.endpoint.portal.context-path}")
    private String apContextPath;

    @Value("${spring.services.endpoint.netsuite-support.appName}")
    private String netsuiteSupportAppName;

    @Value("${spring.services.endpoint.netsuite-support.context-path}")
    private String netsuiteSupportContextPath;

    @Value("${spring.services.endpoint.mail.smtpServer}")
    private String smtpServer;


    public MailClient getMailClient() {
        return new MailClient(eurekaClient.getApplication(mailAppName).getInstances().get(0).getHomePageUrl(),smtpServer);
    }

    public String getPortalUrl() {
        return eurekaClient.getApplication(portalAppName).getInstances().get(0).getHomePageUrl() + apContextPath;
    }

    public String getNetsuiteSupportUrl() {
        return eurekaClient.getApplication(netsuiteSupportAppName).getInstances().get(0).getHomePageUrl() + netsuiteSupportContextPath;
    }

}
