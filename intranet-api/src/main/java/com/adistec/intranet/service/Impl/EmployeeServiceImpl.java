package com.adistec.intranet.service.Impl;

import com.adistec.intranet.configuration.MicroServicesConfiguration;
import com.adistec.intranet.dto.EmployeeDTO;
import com.adistec.intranet.service.EmployeeService;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    private KeycloakRestTemplate keycloakRestTemplate;

    @Autowired
    private MicroServicesConfiguration microServicesConfiguration;

    public List<EmployeeDTO> getAll() {
        return Arrays.stream(keycloakRestTemplate.getForEntity(microServicesConfiguration.getPortalUrl() + "/employee/enabledAndInMap", EmployeeDTO[].class).getBody()).collect(Collectors.toList());
    }

    public EmployeeDTO getById(Long id) throws Exception {
        EmployeeDTO employeeDTO =  keycloakRestTemplate.getForEntity(microServicesConfiguration.getPortalUrl() + "/employee/" +id, EmployeeDTO.class).getBody();
        if(employeeDTO != null){
            return employeeDTO;
        }
        else {
            LOG.error("Cannot find user with id:" + id);
            throw new Exception("Cannot find user with id:" + id);
        }
    }
}
