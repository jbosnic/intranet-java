package com.adistec.intranet.service.Impl;

import com.adistec.intranet.configuration.MicroServicesConfiguration;
import com.adistec.intranet.dto.BranchOfficeDTO;
import com.adistec.intranet.service.BranchOfficeService;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class BranchOfficeServiceImpl implements BranchOfficeService {

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemaplate;

    @Autowired
    private MicroServicesConfiguration microServicesConfiguration;

    public List<BranchOfficeDTO> getAll() {
        try {
            return restTemaplate.exchange(microServicesConfiguration.getNetsuiteSupportUrl() + "/office",HttpMethod.GET,null,new ParameterizedTypeReference<List<BranchOfficeDTO>>() {}).getBody();
        } catch (Exception e) {
            throw e;
        }
    }

    public BranchOfficeDTO getById(Integer id) {
        try {
            return restTemaplate.exchange(microServicesConfiguration.getNetsuiteSupportUrl() + "/office/" + id,HttpMethod.GET,null,BranchOfficeDTO.class).getBody();
        } catch (Exception e) {
            throw e;
        }
    }
}
