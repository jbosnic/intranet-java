package com.adistec.intranet.entity.common;

import java.util.Date;

public interface IContactFormLog {
	public Integer getId();

	public void setId(Integer id);

	public String getFirstName();

	public void setFirstName(String firstName);

	public String getLastName();

	public void setLastName(String lastName);

	public String getEmail();

	public void setEmail(String email);

	public String getPhone();

	public void setPhone(String phone);

	public String getCountry();

	public void setCountry(String country);

	public Date getDateCreated();

	public void setDateCreated(Date dateCreated);

	public String getSource();

	public void setSource(String source);

	public String getPosition();

	public void setPosition(String position);

	public String getCountrySource();

	public void setCountrySource(String countrySource);

	public String getUploadedFile();

	public void setUploadedFile(String uploadedFile);

	public String getEmailReference();

	public void setEmailReference(String emailReference);

	public String getTypeReferred();

	public void setTypeReferred(String typeReferred);

	public String getSearchComments();

	public void setSearchComments(String searchComments);
}
