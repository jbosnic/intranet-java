package com.adistec.intranet.dto;

import java.util.Date;

public class ContactFormLogDTO {

	private Integer id;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String country;
	private Date dateCreated;
	private String source;
	private String position;
	private String countrySource;
	private String uploadedFile;
	private String emailReference;
	private String typeReferred;
	private String searchComments;

	public ContactFormLogDTO(Integer id, String firstName, String lastName, String email, String phone, String country,
			Date dateCreated, String source, String position, String countrySource, String uploadedFile,
			String emailReference, String typeReferred, String searchComments) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.country = country;
		this.dateCreated = dateCreated;
		this.source = source;
		this.position = position;
		this.countrySource = countrySource;
		this.uploadedFile = uploadedFile;
		this.emailReference = emailReference;
		this.typeReferred = typeReferred;
		this.searchComments = searchComments;

	}

	public ContactFormLogDTO() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getCountrySource() {
		return countrySource;
	}

	public void setCountrySource(String countrySource) {
		this.countrySource = countrySource;
	}

	public String getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(String uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public String getEmailReference() {
		return emailReference;
	}

	public void setEmailReference(String emailReference) {
		this.emailReference = emailReference;
	}

	public String getTypeReferred() {
		return typeReferred;
	}

	public void setTypeReferred(String typeReferred) {
		this.typeReferred = typeReferred;
	}

	public String getSearchComments() {
		return searchComments;
	}

	public void setSearchComments(String searchComments) {
		this.searchComments = searchComments;
	}

}
