package com.adistec.intranet.dto;

public class FileWrapper {

    private byte[] bytes;

    private String imgName;

    private String imgUrl;

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public String getImgName() {
        return imgName;
    }

    public void setFileName(String imgName) {
        this.imgName = imgName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
