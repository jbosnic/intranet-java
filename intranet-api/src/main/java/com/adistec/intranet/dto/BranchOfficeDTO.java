package com.adistec.intranet.dto;

public class BranchOfficeDTO {
	private Long id;
	private String name;
	private String country;
	private String city;
	private String phone;
	private String address;
	private String email;
	private Integer left;
	private Integer top;
	private boolean showTextOnLeft;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getLeft() {
		return left;
	}

	public void setLeft(Integer left) {
		this.left = left;
	}

	public Integer getTop() {
		return top;
	}

	public void setTop(Integer top) {
		this.top = top;
	}

	public boolean isShowTextOnLeft() {
		return showTextOnLeft;
	}

	public void setShowTextOnLeft(boolean showTextOnLeft) {
		this.showTextOnLeft = showTextOnLeft;
	}
}
