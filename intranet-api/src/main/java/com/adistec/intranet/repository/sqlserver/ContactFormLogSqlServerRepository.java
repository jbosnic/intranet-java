package com.adistec.intranet.repository.sqlserver;

import com.adistec.intranet.entity.sqlserver.ContactFormLogSqlServer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactFormLogSqlServerRepository extends JpaRepository<ContactFormLogSqlServer, Long> {
	ContactFormLogSqlServer findById(Integer id);
}
